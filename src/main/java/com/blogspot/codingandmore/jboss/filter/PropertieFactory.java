package com.blogspot.codingandmore.jboss.filter;

import java.util.List;

import org.jboss.logging.Logger;

import com.blogspot.codingandmore.jboss.filter.properties.FilePropertieFactory;
import com.blogspot.codingandmore.jboss.filter.properties.FilterProperties;

/**
 * used to get access to the properties
 * 
 * @author wohlgemuth
 * 
 */
public abstract class PropertieFactory {

	private static Logger LOGGER = Logger.getLogger(PropertieFactory.class);

	private static PropertieFactory instance = null;

	/**
	 * returns an instance of the defined factory
	 * 
	 * @param factoryName
	 * @return
	 */
	public static PropertieFactory getInstance(String factoryName)
			throws Exception {
		if (instance == null) {
			if (factoryName != null) {
				instance = (PropertieFactory) Class.forName(factoryName)
						.newInstance();
			} else {
				LOGGER
						.info("please provide a valid filter propertie factory implementation, this can be done over the system enviorement with the key: "
								+ PropertieFactory.class.getName()
								+ " right now we are using the property factory");
				instance = new FilePropertieFactory();

			}
			return instance;
		} else {
			LOGGER.debug("using already existing instance...");
			return instance;
		}
	}

	/**
	 * returns an instance
	 * 
	 * @return
	 */
	public static PropertieFactory getInstance() throws Exception {
		return getInstance(System.getProperty(PropertieFactory.class.getName()));
	}

	/**
	 * returns a list of filter properties
	 * 
	 * @return
	 */
	public abstract List<FilterProperties> getProperties();

	/**
	 * are address permitted by default
	 * 
	 * @return
	 */
	public abstract boolean permitByDefault();
}
