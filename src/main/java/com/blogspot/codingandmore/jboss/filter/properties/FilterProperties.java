package com.blogspot.codingandmore.jboss.filter.properties;

/**
 * used to accept or refuse ips
 * @author wohlgemuth
 *
 */
public class FilterProperties {
	String ip;
	
	boolean permit;

	public FilterProperties(String ip, boolean permit) {
		super();
		this.ip = ip;
		this.permit = permit;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public boolean isPermit() {
		return permit;
	}

	public void setPermit(boolean permit) {
		this.permit = permit;
	}

	@Override
	public String toString() {
		return ip + " - " + permit;
	}
	
	
}
