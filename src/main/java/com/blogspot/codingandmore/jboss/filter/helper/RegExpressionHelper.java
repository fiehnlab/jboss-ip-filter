package com.blogspot.codingandmore.jboss.filter.helper;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

import com.blogspot.codingandmore.jboss.filter.properties.FilterProperties;

/**
 
 * User: wohlgemuth
 * Date: Jan 22, 2010
 * Time: 4:22:43 PM
 */
public class RegExpressionHelper {
    
    public static List<FilterProperties> getRegex(File file) throws FileNotFoundException {

    	List<FilterProperties> properties = new Vector<FilterProperties>();

        Scanner scanner = new Scanner(file);

        //if the file has content, then try to find the key
        while (scanner.hasNextLine()) {
        	String line = scanner.nextLine();
        	
        	if(line.startsWith("#")){
        		//ignore comment
        	}
        	else if(line.indexOf("=") > -1){
        		String[] data = line.split("=");
        		if(data.length == 2){
        			properties.add(new FilterProperties(data[0].trim(), Boolean.parseBoolean(data[1].trim())));
        		}
        	}
        }

        return properties;
    }
}
