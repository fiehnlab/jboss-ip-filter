package com.blogspot.codingandmore.jboss.filter;

import java.io.File;

import junit.framework.TestCase;

import org.jboss.invocation.Invocation;

import com.blogspot.codingandmore.jboss.filter.properties.FilePropertieFactory;

public class SessionInterceptorTest extends TestCase {

	private SessionInterceptor interceptor = null;

	private boolean failed = false;
	
	private String message = "";
	
	private Thread thread = new Thread(new Runnable() {
		
		public void run() {
			try {
				message = "";
				interceptor.invoke(new Invocation());
				failed = false;
				
				
			} catch (NullPointerException e) {
				message = e.getMessage();
				e.printStackTrace();
			} catch (Exception e) {
				failed = true;
				message = e.getMessage();
			}				
		}
	});
	
	public void testInvokeInvocationSuccess() throws Exception {
		interceptor = new SessionInterceptor();
		
		FilePropertieFactory.configuration = new File("src/test/resources/pattern.properties");

		thread.setName("[128.120.136.154]");
		thread.start();
		thread.join();
		
		assertTrue(message,failed == false);
		assertTrue(interceptor.isAcceptedLastRequest());
		assertFalse(interceptor.isAcceptedLastRequestByDefault());
		
	}
	
	public void testInvokeInvocationSuccess2() throws Exception {
		interceptor = new SessionInterceptor();
		
		FilePropertieFactory.configuration = new File("src/test/resources/pattern.properties");

		thread.setName("[10.0.0.1]");
		thread.start();
		thread.join();
		
		assertTrue(message,failed == false);
		assertTrue(interceptor.isAcceptedLastRequest());
		assertFalse(interceptor.isAcceptedLastRequestByDefault());
		
	}

	public void testInvokeInvocationSuccess3() throws Exception {
		interceptor = new SessionInterceptor();
		
		FilePropertieFactory.configuration = new File("src/test/resources/pattern.properties");

		thread.setName("[10.0.0.1:8080]");
		thread.start();
		thread.join();
		
		assertTrue(message,failed == false);
		assertTrue(interceptor.isAcceptedLastRequest());
		assertFalse(interceptor.isAcceptedLastRequestByDefault());
		
	}


	public void testInvokeInvocationFailed() throws Exception {
		interceptor = new SessionInterceptor();
		FilePropertieFactory.configuration = new File("src/test/resources/pattern.properties");

		thread.setName("[128.120.136.132]");
		thread.start();
		thread.join();

		System.out.println("failed message: " + message);
		assertTrue(message,failed);
		assertFalse(interceptor.isAcceptedLastRequest());
		assertFalse(interceptor.isAcceptedLastRequestByDefault());
	}

	public void testInvokeInvocationFailed2() throws Exception {
		interceptor = new SessionInterceptor();
		FilePropertieFactory.configuration = new File("src/test/resources/pattern.properties");

		thread.setName("[128.120.136.132:8080]");
		thread.start();
		thread.join();

		System.out.println("failed message: " + message);
		assertTrue(message,failed);
		assertFalse(interceptor.isAcceptedLastRequest());
		assertFalse(interceptor.isAcceptedLastRequestByDefault());
	}
}
