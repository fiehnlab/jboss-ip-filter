package com.blogspot.codingandmore.jboss.filter.helper;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import org.junit.Test;

import com.blogspot.codingandmore.jboss.filter.properties.FilterProperties;

import junit.framework.TestCase;

public class RegExpressionHelperTest extends TestCase {

	@Test
	public void testGetRegexStringString() throws FileNotFoundException {
		File file = new File("src/test/resources/pattern.properties");
		
		List<FilterProperties> prop = RegExpressionHelper.getRegex(file);
		
		System.out.println(prop.get(0).getIp());
		System.out.println(prop.get(0).isPermit());
		
		assertTrue(prop.get(0).getIp().equals("128.120.136.154"));
		assertTrue(prop.get(0).isPermit());
		
		System.out.println(prop.get(1).getIp());
		System.out.println(prop.get(1).isPermit());
		
		assertTrue(prop.get(2).getIp().equals("\\b(?:\\d{1,3}\\.){3}\\d{1,3}\\b"));
		assertTrue(!prop.get(2).isPermit());
		
		assertTrue("128.120.136.102".matches(prop.get(2).getIp()));
		assertTrue(!prop.get(2).isPermit());
		
		
	}

}
